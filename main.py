from models.MonoWave import MonoWaveDown, MonoWaveUp
from models.helpers import plot_monowave
import numpy as np
import pandas as pd
import requests
import json
import time
from models.helpers import convert_yf_data
from binance.client import Client


from models.WavePattern import WavePattern
from models.WaveRules import Impulse, LeadingDiagonal, Correction, TDWave
from models.WaveAnalyzer import WaveAnalyzer
from models.WaveOptions import WaveOptionsGenerator5
from models.helpers import plot_pattern



# form_data = {
#     "symbol" : "BTCUSDT",
#     "start_date" : "2022-10-01",
#     "end_date": "2022-10-02",
#     "interval": "1h"
# }

# res = requests.get('http://127.0.0.1:8080/v1/spot/price/history',data=form_data)

import csv
from binance.client import Client

SYMBOL = 'LUNAUSDT'

# No API key/secret needed for this type of call
client = Client()

columns = [
    'Date', 'Open', 'High', 'Low', 'Close', 'volume',
    'Close_time', 'quote_asset_volume', 'number_of_trades',
    'taker_buy_base_asset_volume', 'taker_buy_quote_asset_volume',
    'ignore'
]

klines = client.get_historical_klines(SYMBOL, Client.KLINE_INTERVAL_1WEEK, "1 Jan, 2021")

print(len(klines))
print(type(klines[0][0]))
print(klines[0][0])

for i in range (0,len(klines)):
    klines[i][0] = time.strftime('%Y-%m-%d',time.localtime(klines[i][0]/1000))


with open('output.csv', 'w') as f:
    write = csv.writer(f)
    write.writerow(columns)
    write.writerows(klines)


# print(res.text)
# data = json.loads(res.text)
# print(len(data))
# print(data['24'][0]['Symbol'])


# df = pd.read_csv('data/btc-usd_1d.csv')
# lows = np.array(list(df['Low']))
# highs = np.array(list(df['High']))
# dates = np.array(list(df['Date']))

# for i in dates.data:
#     print(i)

# print(list(dates.data))


df = pd.read_csv('output.csv')
idx_start = np.argmin(np.array(list(df['Low'])))

wa = WaveAnalyzer(df=df, verbose=False)
wave_options_impulse = WaveOptionsGenerator5(up_to=15)  # generates WaveOptions up to [15, 15, 15, 15, 15]

impulse = Impulse('impulse')
leading_diagonal = LeadingDiagonal('leading diagonal')
# correction = Correction('correction')
# td_wave = TDWave('td wave')
rules_to_check = [impulse]


wavepatterns_up = set()


# df = pd.read_csv('output.csv')
# lows = np.array(list(df['Low']))
# highs = np.array(list(df['High']))
# dates = np.array(list(df['Date']))


for new_option_impulse in wave_options_impulse.options_sorted:

    waves_up = wa.find_impulsive_wave(idx_start=idx_start, wave_config=new_option_impulse.values)

    if waves_up:
        wavepattern_up = WavePattern(waves_up, verbose=True)

        for rule in rules_to_check:

            if wavepattern_up.check_rule(rule):
                if wavepattern_up in wavepatterns_up:
                    continue
                else:
                    wavepatterns_up.add(wavepattern_up)
                    print(f'{rule.name} found: {new_option_impulse.values}')
                    plot_pattern(df=df, wave_pattern=wavepattern_up, title=str(new_option_impulse))










# date_list = []

# for  i in range (0,len(dates)):
#     date = dates[i].item()
#     date_list.append(time.strftime('%Y-%m-%d',time.localtime(int(date / 1000))))

# dates = np.array(list(date_list))

#VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVv

# mw_up = MonoWaveUp(lows=lows, highs=highs, dates=dates, idx_start=1, skip=3)
# plot_monowave(df, mw_up)

# # find a monowave down from the end of the monowave up
# mw_down = MonoWaveDown(lows=lows, highs=highs, dates=dates, idx_start=mw_up.idx_end, skip=0)
# plot_monowave(df, mw_down)