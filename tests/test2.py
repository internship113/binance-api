import pandas as pd
from pandas.api.types import is_numeric_dtype
import matplotlib.pyplot as plt
from mplfinance.original_flavor import candlestick_ohlc
from matplotlib.dates import date2num
import subprocess
import numpy
import sys

sys.path.append('/home/computerlogy/Workspace/binance-api')

from klines import getCSV

def ewma(data, window):
    return data.ewm(span=window, min_periods=1).mean()

# Read in the CSV file
# df = pd.read_csv('data/output.csv')
symbol = 'BTC'
pair = 'USDT'
getCSV(sysmbol=symbol+pair)
df = pd.read_csv('testCSV.csv')
# Select the Close and Volume columns
df = df[['Date','Open','High','Low','Close']]

# df['Date'] = pd.to_numeric(df['Date'])
# df['Date'] = pd.to_datetime(df['Date'])
df['Date'] = date2num(df['Date'])
df['Open'] = pd.to_numeric(df['Open'])
df['High'] = pd.to_numeric(df['High'])
df['Low'] = pd.to_numeric(df['Low'])
df['Close'] = pd.to_numeric(df['Close'])

# Calculate the EMA of the Close column
df['EMA'] = ewma(df['Close'], 14)

# Create a new column called Wave that contains the difference between the current Close value and the previous Close value
df['Wave'] = df['Close'].shift(1) - df['Close']

# Create a new column called Trend that contains either a 1 or a -1, depending on whether the Wave value is positive or negative
df['Trend'] = df['Wave'].apply(lambda x: 1 if x > 0 else -1)

# Calculate the sum of the Trend values over a window size of 5
df['Wave_Sum'] = df['Trend'].rolling(5).sum()

# Identify the Elliott wave patterns using the Wave_Sum column
df['Elliott_Wave'] = df['Wave_Sum'].apply(lambda x: 'Upward' if x > 0 else 'Downward')

# print(df)

# Select the Open, High, Low, and Close columns
# print(df[['Open', 'High', 'Low', 'Close']])
ohlc = df[['Date','Open', 'High', 'Low', 'Close']]

print(df.to_string())

print(ohlc)

# Convert the data to a list of tuples
data = [tuple(x) for x in ohlc.values]

# Create a figure and an axis
fig, ax = plt.subplots()

# x = numpy.arange(10)
# y = numpy.array([5,3,4,2,7,5,4,6,3,2])
# fig = plt.figure()
# ax = fig.add_subplot(111)
# ax.set_ylim(0,10)
# for i,j in zip(x,y):
#     ax.annotate(str(1),xy=(i,j))
# Plot the candlestick chart
candlestick_ohlc(ax, data,colorup='green',colordown='red')
# symbol = 'BTCUSDT'

# Draw a horizontal line at the value of 100 for each upward wave
df[df['Elliott_Wave'] == 'Upward'].plot(x='Date', y='Close', ax=plt.gca(), kind='line',title=symbol+pair,use_index=True,figsize=[10,5])
upward = 0
for i in df['Elliott_Wave']:
    if i =='Upward':
        upward = upward+1
ax.set_xlabel('Date')
ax.set_ylabel('Price')
# plt.axhline(y=100, color='r', linestyle='-')

# Show the plot
# plt.show()
plt.savefig('plot.png',dpi= 1980)
# subprocess.run(['open', 'plot.png'])

print('Upward wave: ',upward)