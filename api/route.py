from flask import Flask, request as flask_request
import requests
import pandas as pd
import json
from werkzeug.exceptions import HTTPException
import log
from binance.spot import Spot
from datetime import datetime, timezone
import calendar


client = Spot()

log.set_level_log('info')

app = Flask(__name__)
RESTful_version = "v1"


@app.errorhandler(HTTPException)
def handle_http_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    response = e.get_response()
    response.data = json.dumps({
        "error": {
            "code": e.code,
            "error_name": e.name,
            "message": e.description
        }
    })
    response.content_type = "application/json"
    return response


def handle_exception(e, code):
    """Return JSON instead of HTML for HTTP errors."""
    response = json.dumps({
        "error": {
            "code": code,
            "message": str(e)
        }
    })
    return response, code, {'Content-Type': 'application/json'}


@app.route("/{}/healthcheck".format(RESTful_version), methods=["GET"])
def healthcheck():
    log.error("Healthcheck")
    return "Welcome to Binance spot price API  via flask framework.", 200, {'Content-Type': 'text'}


@app.route("/{}/spot/price/now".format(RESTful_version), methods=["GET"])
def get_price():
    try:
        required_parameter = ['symbol','interval']
        output = {}
        for param in required_parameter:
            if param not in flask_request.form:
                raise Exception(
                    "Request parameter '{}' not found".format(param))

        symbol = flask_request.form['symbol']
        interval = flask_request.form['interval']
        output = client.klines(symbol=symbol, interval=interval, limit=1)
        client_time = client.time()['serverTime']
        output = add_detail(output)
        log.info("Output: "+ str(output))
        res = {
            "time": datetime.utcfromtimestamp(client_time/1000).strftime('%Y-%m-%d %H:%M:%S'),
            "sysmbol": symbol,
            "data":  output
        }

        return res, 200, {'Content-Type': 'application/json'}
    except Exception as e:
        message = 'Error : {}'.format(str(e))
        log.error(message)
        return handle_exception(e=message, code=400)


def add_detail(data):
    res = [{
        "Kline open time": datetime.utcfromtimestamp(data[0][0]/1000).strftime('%Y-%m-%d %H:%M:%S'),
        "Open price": data[0][1],
        "High price": data[0][2],
        "Low price": data[0][3],
        "Close price": data[0][4],
        "Volume": data[0][5],
        "Kline close time": datetime.utcfromtimestamp(data[0][6]/1000).strftime('%Y-%m-%d %H:%M:%S'),
        "Quote asset volume": data[0][7],
        "Number of trade": data[0][8],
        "Taker buy base asset volume": data[0][9],
        "Taker buy quote asset volume": data[0][10],
        "Unused field. Ignore.": data[0][11],
    }]
    return res


@app.route("/{}/spot/price/history".format(RESTful_version), methods=["GET"])
def get_history_price():
    try:
        required_parameter = ['symbol', 'start_date', 'end_date', 'interval']
        for param in required_parameter:
            if param not in flask_request.form:
                raise Exception(
                    "Request parameter '{}' not found".format(param))

        symbol = flask_request.form['symbol']
        start = flask_request.form['start_date']
        end = flask_request.form['end_date']
        interval = flask_request.form['interval']
        output =get_klines_iter(symbol=symbol, interval=interval, start=start, end=end)
        return output, 200, {'Content-Type': 'application/json'}
    except Exception as e: 
        message = 'Error : {}'.format(str(e))
        log.error(message)
        return handle_exception(e=message, code=400)


def get_klines_iter(symbol, interval, start, end=None):
    # start and end must be isoformat YYYY-MM-DD
    # the maximum records is 1000 per each Binance API call
    df = pd.DataFrame()

    if start is None:
        log.error('start time must not be None')
        return
    start = calendar.timegm(datetime.fromisoformat(start).timetuple()) * 1000

    if end is None:
        dt = datetime.now(timezone.utc)
        utc_time = dt.replace(tzinfo=timezone.utc)
        end = int(utc_time.timestamp()) * 1000
        return
    else:
        end = calendar.timegm(datetime.fromisoformat(end).timetuple()) * 1000
    last_time = None

    while len(df) == 0 or (last_time is not None and last_time < end):
        url = 'https://api.binance.com/api/v3/klines?symbol=' + \
              symbol + '&interval=' + interval + '&limit=1000'
        if (len(df) == 0):
            url += '&startTime=' + str(start)
        else:
            url += '&startTime=' + str(last_time)

        url += '&endTime=' + str(end)
        res = requests.get(url)
        data = res.json()
        list = {}
        for i in range(0, len(data)):
            list[i]=[{
                        "Symbol":symbol,
                        "Kline open time": datetime.utcfromtimestamp(data[i][0]/1000).strftime('%Y-%m-%d %H:%M:%S'),
                        "Open price": data[i][1],
                        "High price": data[i][2],
                        "Low price": data[i][3],
                        "Close price": data[i][4],
                        "Volume": data[i][5],
                        "Kline close time": datetime.utcfromtimestamp(data[i][6]/1000).strftime('%Y-%m-%d %H:%M:%S'),
                        "Quote asset volume": data[i][7],
                        "Number of trade": data[i][8],
                        "Taker buy base asset volume": data[i][9],
                        "Taker buy quote asset volume": data[i][10],
                        "Unused field. Ignore.": data[i][11],
                        }]
        return json.dumps(list)


log.info('READY!!!')
if __name__ == '__main__':
    app.debug = True
    app.run(host='127.0.0.1', port='8080', debug=True)
