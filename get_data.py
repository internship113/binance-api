import yfinance as yf

from models.helpers import convert_yf_data

df = yf.download(tickers='BTC-USD',
                 interval="1d",
                 start="2020-12-01")

convert_yf_data(df).to_csv(r'data/output.csv', sep=",", index=False)

# import csv
# import time
# from binance.client import Client

# SYMBOL = 'BTCUSDT'

# # No API key/secret needed for this type of call
# client = Client()

# columns = [
#     'Date', 'Open', 'High', 'Low', 'Close', 'volume',
#     'Close_time', 'quote_asset_volume', 'number_of_trades',
#     'taker_buy_base_asset_volume', 'taker_buy_quote_asset_volume',
#     'ignore'
# ]

# klines = client.get_historical_klines(SYMBOL, Client.KLINE_INTERVAL_1WEEK, "1 Jan, 2021")

# print(len(klines))
# print(type(klines[0][0]))
# print(klines[0][0])

# for i in range (0,len(klines)):
#     klines[i][0] = time.strftime('%Y-%m-%d',time.localtime(klines[i][0]/1000))


# with open('output.csv', 'w') as f:
#     write = csv.writer(f)
#     write.writerow(columns)
#     write.writerows(klines)


